# Weather



## Instalacion del proyecto

Como libreria de dependecia se tomo la decicion de usar cocoapods ya que es el mas usado y conocido por la mayoria de desarrolladores IOS.

Para la instalacion nos vamos a la ruta donde se encuentre nuestro proyecto, donde esta ubicado el podfile, desde el terminal ejecutaremos el siguiente comando desde la terminal:  

```
pod install
```
Si llegamos a presentar algun problema se debera ejecutar el siguiente comando para limpiar y reinstallar todas las depedencia de nuevo:

```
pod deintegrat
pod install
```
## Explicacion de la estructura del proyecto


### ARQUITECTURA
___
Para este proyeco se decidio utilizar **MVVM** el cual es muy usado con SwiftUI y combine, a este proyecto se le añadieron dos capas mas que son los interactors y repository son dos capas las cuales nos permite desacoplar el codigo y hacerlo mas reutilizable, con el fin de tambien tener una arquitecura limpia **Clean Architecture** 

### Proyecto
___

Esta aplicacion tiene 3 mosulos importante los cuales son el **Splash, Search y Detail** los cuales conforman esta aplicacion, pero tambien contamos con una structura en el proyecto la cual sera explicada a continuacion  

![imagen](climate/ImageDoc/splash.png)

### Repository
___
esta es una de las clase de mas bajo nivel que encontramos  dentro de la aplicacio, su funcion basicamente es de traer, guardar, eliminar o actualizar la informacion que se trabaja dentro de la aplicacion, un corta explicacion seria la siguiente:

Dentro de la aplicacion contamos con apiRest, FireBase, CoreData o coalquier otro tipo defuente de informacion, esta capa esta hecha para unificar y comunicarse con cualquiera de estas y estandarizar como se optiene la informcacion   

![imagen](climate/ImageDoc/Repositoies.png)

### Interactor
___
Este interactor es similar a una ficha de rompecabeza esta hecho con el fin de ser reutilizado solo debe contener un metodo el cual se encarga de encapsularlo para que sea util dentro de los ViewModel que los necesite, contiene las invocaciones o consume que se hace a un Repository

![imagen](climate/ImageDoc/Interactor.png)

### Modulos
___
En esta capa vemos las partes visible y funcionales de la aplicacion, se ha dividido en tres partes **View - Model - ViewModel** el objetivo es usar SwiftUi para que cada uno de estos se puedan utilizar, las vistas utilizan un generico el cual perite por medio de OOP poder remplazar cada una de las dependecia deacuerdo a la necesidad del proyecto.  

![imagen](climate/ImageDoc/Modulos.png)

### Injeccion de dependecia
___
Para la injeccion de dependecia se a utilizado resolve la cual es la libreria la cual mediante de grafos nos permite inicializar nuestras vistas y realizar el proceso de las pruebas unitarias mas facil.

![imagen](climate/ImageDoc/Injection.png)

### Pruebas unitarias
___
Las pruebas unitarias se realiza creando un mock del Repository eso nos permite realizar una prueba de cada uno de nuestro ViewModel's  sin tener la dependenica del back se prueba la funciones clave de la aplicacion. Esta se divide en tres partes el mock princial que consiste en el Repo y los stups los cuales son los interactor y la prueba en si de la logica de la aplicacion.

![imagen](climate/ImageDoc/ModulosTest.png)