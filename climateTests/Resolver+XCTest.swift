//
//  Resolver+XCTest.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
import Resolver
@testable import climate

extension Resolver {
    // MARK: - Mock Container
  static var mock = Resolver(parent: .main)

  // MARK: - Register Mock Services
    static func registerMockServices() {
    root = Resolver.mock
    defaultScope = .graph

    Resolver.mock.register { WeatherRepositoryMock() }
      .implements(WeatherRepositoryMock.self)
    }
}
