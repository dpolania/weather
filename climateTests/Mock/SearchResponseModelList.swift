//
//  SearchResponseModelList.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
@testable import climate

struct SearchResponseModelList {
    var data : [SearchResponseModel] {
        get {
            return [SearchResponseModel(id: 1,
                                       name: "TestName",
                                       region: "TextRegion",
                                       country: "TextCountry",
                                       lat: 12.0,
                                       lon: 12.0,
                                       url: "test")]
        }
    }
}
