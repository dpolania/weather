//
//  ForecastResponseModuleList.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//
import Foundation
@testable import climate

struct ForecastResponseModuleList {
    var data : ForecastResponseModule {
        get {
            return ForecastResponseModule(location: LocationModel(name: "Test",
                                                                  region: "Test",
                                                                  country: "Test",
                                                                  lat: 10,
                                                                  lon: 10,
                                                                  tz_id: "Test",
                                                                  localtime_epoch: 10,
                                                                  localtime: "Test"),
                                          current: CurrentModel(
                                            last_updated_epoch: 10,
                                            last_updated: "Test",
                                            temp_c: 10,
                                            temp_f: 10,
                                            is_day: 10,
                                            condition: ConditionModel(
                                                text: "Test",
                                                icon: "Test",
                                                code: 1012),
                                            humidity: 12,
                                            cloud: 10,
                                            uv: 10), forecast: ForecastModel(forecastday: [ForecastdayModel(date: "Text",
                                                                                                            day: DayModel(
                                                                                                                maxtemp_c: 10,
                                                                                                                maxtemp_f: 10,
                                                                                                                mintemp_c: 10,
                                                                                                                mintemp_f: 10,
                                                                                                                condition: ConditionModel(
                                                                                                                    text: "",
                                                                                                                    icon: "",
                                                                                                                    code: 110),
                                                                                                                uv: 10),
                                                                                                            hour: [
                                                                                                                HourModel(
                                                                                                                    time: "",
                                                                                                                    temp_c: 10,
                                                                                                                    condition: ConditionModel(
                                                                                                                        text: "",
                                                                                                                        icon: "",
                                                                                                                        code: 110),
                                                                                                                    uv: 10
                                                                                                                )
                                                                                                            ]
                                                                                                           )]
                                                                            )
            )
        }
    }
}
