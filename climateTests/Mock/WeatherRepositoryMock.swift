//
//  WeatherRepositoryMock.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
import Combine
import Resolver
@testable import climate

final class WeatherRepositoryMock: WeatherRepositoryType {
    private var  decoder = JSONDecoder()
    @Injected private var  network : NetworUtil
    
    func getPlaceSearch(search: String) -> AnyPublisher<[SearchResponseModel], Error> {
        let subject =  CurrentValueSubject<[SearchResponseModel], Error>(SearchResponseModelList().data)
        return subject.eraseToAnyPublisher()
    }
    
    func getDeatilWeather(text: String, days: Int) -> AnyPublisher<ForecastResponseModule, Error> {
        let subject =  CurrentValueSubject<ForecastResponseModule, Error>(ForecastResponseModuleList().data)
            return subject.eraseToAnyPublisher()
    }
}
