//
//  ForecastInteractorTypeStup.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
import Combine
@testable import climate


final class ForecastInteractorTypeStup: DetailInteractorType {
    static var responseHandler : InteractorStubCase<Any> = .succes({})
    
    func getDeatilWeather(name: String) -> AnyPublisher<climate.ForecastResponseModule, Error> {
        let object = ForecastResponseModuleList().data
        var publisher =  CurrentValueSubject<ForecastResponseModule, Error>(object)
        switch SearchInteractorTypeStup.responseHandler{
        case .succes(let handler):
            publisher = CurrentValueSubject<ForecastResponseModule, Error>(handler() as? ForecastResponseModule ?? object)
        case .failure(let error):
            publisher.send(completion: .failure(error()))
        }
        return publisher.eraseToAnyPublisher()
    }
}
