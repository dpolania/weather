//
//  SearchInteractorTypeStup.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
import Combine
@testable import climate

enum GetPopularSearchError : Error{
    case badRequest
    case itemNotFound
}
enum InteractorStubCase<T>{
    case succes(()->T)
    case failure(()->Error)
}

final class SearchInteractorTypeStup: SearchInteractorType {
    
    static var responseHandler : InteractorStubCase<Any> = .succes({})
    
    func getPlaceSearch(search: String) -> AnyPublisher<[climate.SearchResponseModel], Error> {
        let object = SearchResponseModelList().data
        var publisher =  CurrentValueSubject<[SearchResponseModel], Error>(object)
        switch SearchInteractorTypeStup.responseHandler{
        case .succes(let handler):
            publisher = CurrentValueSubject<[SearchResponseModel], Error>(handler() as? [SearchResponseModel] ?? object)
        case .failure(let error):
            publisher.send(completion: .failure(error()))
        }
        return publisher.eraseToAnyPublisher()
    }
}
