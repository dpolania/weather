//
//  SearchViewModelTest.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import XCTest
@testable import climate

final class SearchViewModelTest: XCTestCase {

    //Subjet under test: SUT
        private var sut : SearchViewModel!
        private var searchInteractorTypeStup : SearchInteractorTypeStup!
        
        override func setUpWithError() throws {
            searchInteractorTypeStup = SearchInteractorTypeStup()
            let dependencies = SearchViewModel.InputDependences(searchInteractor: searchInteractorTypeStup)
            sut = SearchViewModel(dependencies: dependencies)
        }

        override func tearDownWithError() throws {
            searchInteractorTypeStup = nil
            sut = nil
        }

        func testSearch_whenSucces(){
            let expectation = XCTestExpectation(description: "Search")
            SearchInteractorTypeStup.responseHandler = .succes({
                SearchResponseModelList().data
            })
            sut.onAppear()
            sut.state.textSearch = "text"
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                XCTAssertTrue(self.sut.state.data.first?.country != nil)
                expectation.fulfill()
            }
            wait(for: [expectation], timeout: 10)
        }

        func testSearch_whenFailure(){
            let expectation = XCTestExpectation(description: "error badRequest")
            SearchInteractorTypeStup.responseHandler = .failure({
                GetPopularSearchError.badRequest
            })
            sut.state.textSearch = "text"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                XCTAssertFalse(self.sut.state.data.first?.country != nil)
                expectation.fulfill()
            }
            wait(for: [expectation], timeout: 0.6)
        }
}
