//
//  DetailViewModelTest.swift
//  climateTests
//
//  Created by dpolania on 13/06/23.
//

import Foundation
import XCTest
@testable import climate

final class DetailViewModelTest: XCTestCase {

    //Subjet under test: SUT
        private var sut : DetailViewModel!
        private var forecastInteractorTypeStup : ForecastInteractorTypeStup!
        
        override func setUpWithError() throws {
            forecastInteractorTypeStup = ForecastInteractorTypeStup()
            let dependencies = DetailViewModel.InputDependences(detailInteractor: forecastInteractorTypeStup)
            sut = DetailViewModel(dependencies: dependencies)
        }

        override func tearDownWithError() throws {
            forecastInteractorTypeStup = nil
            sut = nil
        }

        func testDetail_whenSucces(){
            let expectation = XCTestExpectation(description: "get detail")
            SearchInteractorTypeStup.responseHandler = .succes({
                SearchResponseModelList().data
            })
            sut.onAppear(name: "test")
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                XCTAssertTrue(self.sut.state.data?.current.last_updated != nil)
                expectation.fulfill()
            }
            wait(for: [expectation], timeout: 10)
        }

        func testDetail_whenFailure(){
            let expectation = XCTestExpectation(description: "error badRequest")
            SearchInteractorTypeStup.responseHandler = .failure({
                GetPopularSearchError.badRequest
            })
            sut.onAppear(name: "test")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                XCTAssertFalse(self.sut.state.data?.current.last_updated != nil)
                expectation.fulfill()
            }
            wait(for: [expectation], timeout: 0.6)
        }
}
