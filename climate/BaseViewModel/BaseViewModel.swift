//
//  BaseViewModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation
import Combine

open class BaseViewModel {
    enum ErrorViewModel {
        case general
        case timeout
        case server
        case none
        case internetConection
    }
    var errorService : ErrorViewModel = .none
    private var constant : ConstantString = ConstantString()
    
    func evaluateError(_ error : Error) -> (Bool,String)?{
        var msg = ""
        var viewMessage : Bool = false
        
        if error._code == -1000 {
            errorService = .none
        } else {
            errorService = .general
        }
        
        switch errorService {
        case .general:
            viewMessage = true
            msg = constant.returnString(key: .popUpsHttpError, table: .details)
        case .timeout:
            viewMessage = true
            msg = constant.returnString(key: .popUpsHttpError, table: .details)
        case .server:
            viewMessage = true
            msg = constant.returnString(key: .popUpsHttpError, table: .details)
        case .none:
            viewMessage = false
            msg = constant.returnString(key: .popUpsHttpError, table: .details)
        case .internetConection:
            viewMessage = true
            msg = constant.returnString(key: .popUpsHttpError, table: .details)
        }
        
        return (viewMessage,msg)
    }
    
    private func transformError(_ error : Error){
        switch error._code {
        case -1009:
            errorService = .internetConection
        default:
            errorService = .general
        }
    }
}
