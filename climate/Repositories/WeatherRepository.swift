//
//  WeatherRepository.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation
import Combine
import Resolver

protocol WeatherRepositoryType {
    func getPlaceSearch(search: String) -> AnyPublisher<[SearchResponseModel], Error>
    func getDeatilWeather(text: String, days: Int) -> AnyPublisher<ForecastResponseModule, Error>
}

final class WeatherRepository: WeatherRepositoryType {
    private var  decoder = JSONDecoder()
    @Injected private var  network : NetworUtil
    
    func getPlaceSearch(search: String) -> AnyPublisher<[SearchResponseModel], Error> {
        let request = network.requestAPI(url: ApiServices.search(search),method: .get)
        return request
            .decode(type: [SearchResponseModel].self, decoder: decoder)
            .eraseToAnyPublisher()
    }
    
    func getDeatilWeather(text: String, days: Int) -> AnyPublisher<ForecastResponseModule, Error> {
        let request = network.requestAPI(url: ApiServices.forecast(days, text),method: .get)
        return request
            .decode(type: ForecastResponseModule.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
}

