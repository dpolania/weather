//
//  NetworUtil.swift
//  CondorFilmsRemaster (iOS)
//
//  Created by David Polania on 19/04/22.
//

import Foundation
import Combine

protocol Network{
    func requestAPI(url service  : String,
                    body : [String:Any],
                    header : [String:String],
                    method : httpMethod) -> AnyPublisher<Data, URLSession.DataTaskPublisher.Failure>
}

public enum httpMethod : String {
    case post   = "POST"
    case get    = "GET"
    case put    = "PUT"
    case delete = "DELETE"
}

public class NetworUtil : Network{
    public init(){
    }
    public func requestAPI(url service  : String,
                    body : [String:Any] = [:],
                    header : [String:String] = [:],
                    method : httpMethod = .get) -> AnyPublisher<Data, URLSession.DataTaskPublisher.Failure>{

        guard let url = URL(string: service) else {
            return Fail(error: URLSession.DataTaskPublisher.Failure.init(.badURL)).eraseToAnyPublisher()
        }
        
        let session = URLSession.shared


        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(1000)
        request.httpMethod = method.rawValue
        
        if body.count > 0 {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) // pass dictionary to data object and set it as request body
            } catch let error {
                print(error.localizedDescription)            }
        }
        
        if header.count > 0 {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            header.forEach{
                request.addValue($0.0, forHTTPHeaderField: $0.1)
            }
        }
        return session.dataTaskPublisher(for: request).map(\.data).eraseToAnyPublisher()
        
    }
}
