//
//  Colors.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import Foundation
import SwiftUI

extension Color {
    static let primaryApp = Color("Primary")
    static let pastelPurpleApp = Color("PastelPurple")
}
