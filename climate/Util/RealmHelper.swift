////
////  BaseViewModel.swift
////  climate
////
////  Created by dpolania on 9/06/23.
////
///Implementar a futuru para guardar lugares en la app
//
//import Foundation
//import RealmSwift
//
//protocol IRealmHelper{
//    func add<T:Object>(data:T)
//    func delete<T:Object>(data:T,action:@escaping(ResultOperation)->Void)
//    func deleteAll<Element: RealmFetchable>(_ type: Element.Type)
//    func allData<Element: RealmFetchable>(_ type: Element.Type) -> Results<Element>
//    func selectedData<Element: RealmFetchable>(_ type: Element.Type, query:String)-> Results<Element>
//}
//
///**
// Manager para guardar los parametros utilizado en al app, en sus distintos
// modulos
// */
//class RealmHelper:IRealmHelper {
//    func deleteAll<Element>(_ type: Element.Type) where Element : RealmFetchable {
//        let realm = try! Realm()
//        let data = realm.objects(Element.self)
//        try! realm.write {
//            data.forEach { item in
//                realm.delete(item as! ObjectBase)
//            }
//        }
//    }
//
//
//    func allData<Element: RealmFetchable>(_ type: Element.Type) -> Results<Element> {
//        let realm = try! Realm()
//
//        let data = realm.objects(Element.self)
//        return data
//    }
//
//    func add<T>(data: T) where T : Object {
//        let realm = try! Realm()
//
//        try! realm.write {
//            realm.add(data)
//        }
//    }
//
//    func delete<T>(data: T,action:@escaping(ResultOperation)->Void) where T : Object {
//        let realm = try! Realm()
//        do {
//            try! realm.write {
//                realm.delete(data)
//            }
//            action(.success)
//        }catch{
//                action(.error)
//        }
//    }
//
//    func selectedData<Element: RealmFetchable>(_ type: Element.Type, query:String)-> Results<Element>{
//        let realm = try! Realm()
//        let data = realm.objects(Element.self).filter(query)
//        return data
//    }
//
//}
//
//enum ResultOperation {
//    case success
//    case error
//    case exist
//}
