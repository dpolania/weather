//
//  BaseViewModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation

enum TableContentString:String{
    case details = "Details"
    case home    = "Home"
}

enum KeysContentString : String{
    case popUpsTitle            = "popUpsTitle"
    case popDescription         = "popDescription"
    case popUpsTitleWarnning    = "popUpsTitleWarnning"
    case popUpsExistData        = "popUpsExistData"
    case popUpTitleRealm        = "popUpTitleRealm"
    case popUpDetailRealm       = "popUpDetailRealm"
    case popUpCodeTitle         = "popUpCodeTitle"
    case popUpsHttpError        = "popUpsHttpError"
    case titleHome              = "title_home"
    case favoriteButton         = "favorite_button"
    case page                   = "page"
    case okSalve               = "okSalve"
}
/**Fabrica de textos utilizados en la app con los archivos que se encuentra en /Strings*/
class ConstantString{
    func returnString(key:KeysContentString,table:TableContentString) -> String {
        let  result = Bundle.main.localizedString(forKey: key.rawValue, value: nil, table: table.rawValue)
        return result
    }
}
