//
//  BaseViewModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import SwiftUI
import Lottie

struct LottieView: UIViewRepresentable {
    var name = "weather"
    var loopMode: LottieLoopMode = .loop
    var onAnimation : (Bool)->Void?
    var progress : CGFloat = 1

    func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
        let view = UIView(frame: .zero)

        let animationView = LottieAnimationView()
        let animation = LottieAnimation.named(name)
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = loopMode
        animationView.play(toProgress: AnimationProgressTime(progress)) { finishAnimation in
            onAnimation(finishAnimation)
        }

        animationView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(animationView)
        NSLayoutConstraint.activate([
            animationView.heightAnchor.constraint(equalTo: view.heightAnchor),
            animationView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])

        return view
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {
    }
}
