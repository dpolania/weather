//
//  BaseViewModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import UIKit

extension UIImageView {
    /**Descarga una imagen que proviene de una URL*/
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

class ImageDowload {
    /**Descarga una imagen que proviene de una URL*/
    func downloaded(from url: URL, dowload : @escaping (UIImage)->Void){
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
                dowload(image)
            }.resume()
    }
    func downloaded(from link: String, dowload : @escaping (UIImage)->Void) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url){ image in
            dowload(image)
        }
    }
}
