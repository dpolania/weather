//
//  ColorRandom.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import Foundation
import SwiftUI

struct ColorRandom {
    private static var lastNumber = -1
    
    enum PastelColors: String {
        case pastelBlue = "PastelBlue"
        case pastelGreen = "PastelGreen"
        case pastelOrange = "PastelOrange"
        case pastelPink = "PastelPink"
        case pastelPurple = "PastelPurple"
        case pastelPurpleDark = "PastelPurpleDark"
        case pastelYellow = "PastelYellow"
        case pasteRed = "PasteRed"
    }
    
    let arrayAllColors: [PastelColors] = [.pastelBlue,
                                         .pastelBlue,
                                         .pastelGreen,
                                         .pastelOrange,
                                         .pastelPink,
                                         .pastelPurple,
                                         .pastelPurpleDark,
                                         .pastelYellow,
                                         .pasteRed]
        
    func getColor() -> Color {
        let totalColor = arrayAllColors.count
        let randomInt = Int.random(in: 1..<totalColor)
        guard randomInt != ColorRandom.lastNumber else {
            return getColor()
        }
        ColorRandom.lastNumber = randomInt
        return Color(arrayAllColors[randomInt].rawValue)
    }
}
