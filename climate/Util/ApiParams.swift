//
//  BaseViewModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation

/**
 Parametros para el uso del apiRest
 */
struct ApiParams {
    static let apiKey = "c9c12f28c2f24bb4a01223746230906"
    static let baseURL = "https://api.weatherapi.com/v1/"
    static var urlImage = "https://image.tmdb.org/t/p/w500"
}

/**Metodos del servicio apiRest*/
struct ApiServices {
    typealias textSearch = String
    typealias numberDays = Int
    typealias name = String
    
    static let search: (textSearch) -> String = { text in
        return "\(ApiParams.baseURL)search.json?key=\(ApiParams.apiKey)&q=\(text)"
    }
    
    static var forecast: (numberDays, name)-> String = { days, name in
        return "\(ApiParams.baseURL)forecast.json?key=\(ApiParams.apiKey)&q=\(name)&days=\(days)&lang=es"
    }
}
