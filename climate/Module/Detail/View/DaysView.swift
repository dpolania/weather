//
//  DaysView.swift
//  climate
//
//  Created by dpolania on 13/06/23.
//

import SwiftUI

struct DaysView: View {
    var forecastdayModel: ForecastdayModel
    let colorRandom = ColorRandom()
    var body: some View {
        VStack {
            VStack {
                HStack(alignment: .top) {
                    VStack {
                        HStack {
                            Text("Fecha:")
                                .font(.custom("Righteous-Regular",
                                              size: 24))
                                .foregroundColor(.black)
   
                            Text(forecastdayModel.date)
                                .font(.custom("Righteous-Regular",
                                              size: 18))
                                .foregroundColor(.black)
                            Spacer()
                        }
                        
                        HStack {
                            Text("Max Temperatura:")
                                .font(.custom("Montserrat-Medium",
                                              size: 18))
                                .foregroundColor(.black)
                            
                            Text("\(String(forecastdayModel.day.maxtemp_c))c")
                                .font(.custom("Montserrat-Light",
                                              size: 18))
                                .foregroundColor(.black)
                            Spacer()
                        }
                    }
                    
                    Spacer()
                    
                    HStack(alignment:.center) {
                        Text("UV:")
                            .font(.custom("Montserrat-Medium",
                                          size: 18))
                            .foregroundColor(.black)
                        
                                Text("\(Int(forecastdayModel.day.uv))")
                            .font(.custom("Montserrat-Light",
                                          size: 18))
                            .foregroundColor(.black)
                        Spacer()
                    }.frame(width: 70)
                }
                
                
                VStack {
                    HStack {
                        Text("Min Temperatura:")
                            .font(.custom("Montserrat-Medium",
                                          size: 18))
                            .foregroundColor(.black)
                        
                        Text("\(String(forecastdayModel.day.mintemp_c))c")
                            .font(.custom("Montserrat-Light",
                                          size: 18))
                            .foregroundColor(.black)
                        Spacer()
                    }
                }
                
                Spacer().frame(height: 18)
                
                VStack(alignment: .trailing) {
                    HStack(alignment:.bottom){
                        Text(forecastdayModel.day.condition.text ?? "Nublado")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.black)
                        
                        Spacer()
                        
                        AsyncImage(url: URL(string: "https:\(forecastdayModel.day.condition.icon)")) { image in
                            image
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        } placeholder: {
                            Image("Empty")
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        }.frame(width: 50)
                    }
                }
                GraphView(temperatureGraph: forecastdayModel.hour.map({TemperatureGraph(temp_c: $0.temp_c, time: $0.time)}))
                .frame(maxHeight: 100)
            }
            .padding(.vertical)
            .padding(.horizontal)
            .background(colorRandom.getColor())
            .cornerRadius(20)
            
            Spacer()
        }
    }
}

struct DaysView_Previews: PreviewProvider {
    static var previews: some View {
        DaysView(forecastdayModel: ForecastdayModel(date: "Text", day: DayModel(maxtemp_c: 10, maxtemp_f: 10, mintemp_c: 10, mintemp_f: 10, condition: ConditionModel(text: "", icon: "", code: 110), uv: 10), hour: [HourModel(time: "", temp_c: 10, condition: ConditionModel(text: "", icon: "", code: 110), uv: 10)]))
    }
}
