//
//  DetailView.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import SwiftUI

struct DetailView<ViewModel> : View where ViewModel : DetailViewModel {
    @StateObject var viewModel: ViewModel
    @State private var orientation = UIDeviceOrientation.unknown
    
    var name: String
    
    var body: some View {
        return Group {
            if orientation.isPortrait {
                portraid
            } else if orientation.isLandscape {
                landScape
            } else if orientation.isFlat {
                portraid
            } else {
                portraid
            }
        }.alert(viewModel.state.errorMessage, isPresented: $viewModel.state.viewMessage) {
            Button("Aceptar", role: .cancel) {}
        }
        .onRotate { newOrientation in
            orientation = newOrientation
        }
        
    }
    
    @ViewBuilder
    var portraid : some View {
        VStack {
            VStack {
                HStack(alignment: .top) {
                    VStack {
                        HStack {
                            Text(viewModel.state.data?.location.name ?? "")
                                .font(.custom("Righteous-Regular",
                                              size: 48))
                                .foregroundColor(.white)
                            Spacer()
                        }
                        
                        HStack {
                            Text(viewModel.state.data?.location.country ?? "")
                                .font(.custom("Montserrat-Light",
                                              size: 28))
                                .foregroundColor(.white)
                            Spacer()
                        }
                    }
                    
                    Text("\(String(viewModel.state.data?.current.temp_c ?? 18))c")
                        .font(.custom("Montserrat-Light",
                                      size: 28))
                        .foregroundColor(.white)
                }
                
                Spacer().frame(height: 18)
                
                VStack {
                    HStack(alignment:.bottom){
                        Text("Fecha de consulta:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(viewModel.state.data?.current.last_updated ?? "")
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                    
                    HStack(alignment:.bottom){
                        Text("Covertura de nuves:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(String(viewModel.state.data?.current.cloud ?? 10))
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                    
                    HStack(alignment:.bottom){
                        Text("Humedad:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(String(viewModel.state.data?.current.humidity ?? 10))
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                }
                
                Spacer().frame(height: 18)
                
                VStack(alignment: .trailing) {
                    HStack(alignment:.bottom){
                        Text(viewModel.state.data?.current.condition.text ?? "")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                        
                        AsyncImage(url: URL(string: "https:\(viewModel.state.data?.current.condition.icon ?? "")")) { image in
                            image
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        } placeholder: {
                            Image("Empty")
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        }.frame(width: 50)
                    }
                }
            }
            .padding(.horizontal)
            .background(
                LinearGradient(gradient: Gradient(colors: [.pastelPurpleApp, .purple]), startPoint: .leading, endPoint: .trailing)
            )
            .cornerRadius(20)
            
            ScrollView(.horizontal) {
                LazyHStack {
                    if let forecastday = viewModel.state.data?.forecast.forecastday {
                        ForEach(forecastday, id:\.date){ result in
                            DaysView(forecastdayModel: result)
                        }
                    }
                }
                .background(Color.clear)
                .padding()
                
            }
            .background(Color(UIColor.systemGroupedBackground))
            Spacer()
        }
        .padding(.horizontal,16)
        .onAppear {
            viewModel.onAppear(name: name)
        }
    }
    
    @ViewBuilder
    var landScape : some View {
        HStack {
            VStack {
                HStack(alignment: .top) {
                    VStack {
                        HStack {
                            Text(viewModel.state.data?.location.name ?? "Bogota")
                                .font(.custom("Righteous-Regular",
                                              size: 48))
                                .foregroundColor(.white)
                            Spacer()
                        }
                        
                        HStack {
                            Text(viewModel.state.data?.location.country ?? "Colombia")
                                .font(.custom("Montserrat-Light",
                                              size: 28))
                                .foregroundColor(.white)
                            Spacer()
                        }
                    }
                    
                    Text("\(String(viewModel.state.data?.current.temp_c ?? 18))c")
                        .font(.custom("Montserrat-Light",
                                      size: 28))
                        .foregroundColor(.white)
                }
                
                Spacer().frame(height: 18)
                
                VStack {
                    HStack(alignment:.bottom){
                        Text("Fecha de consulta:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(viewModel.state.data?.current.last_updated ?? "2020/02/02")
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                    
                    HStack(alignment:.bottom){
                        Text("Covertura de nuves:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(String(viewModel.state.data?.current.cloud ?? 10))
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                    
                    HStack(alignment:.bottom){
                        Text("Humedad:")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Text(String(viewModel.state.data?.current.humidity ?? 10))
                            .font(.custom("Montserrat-Regular",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                    }
                }
                
                Spacer().frame(height: 18)
                
                VStack(alignment: .trailing) {
                    HStack(alignment:.bottom){
                        Text(viewModel.state.data?.current.condition.text ?? "Nublado")
                            .font(.custom("Montserrat-Bold",
                                          size: 18))
                            .foregroundColor(.white)
                        
                        Spacer()
                        
                        AsyncImage(url: URL(string: "https:\(viewModel.state.data?.current.condition.icon ?? "")")) { image in
                            image
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        } placeholder: {
                            Image("Empty")
                                .resizable()
                                .frame(width: 25, height: 25)
                                .ignoresSafeArea()
                        }.frame(width: 50)
                    }
                }
            }
            .padding(.horizontal)
            .background(
                LinearGradient(gradient: Gradient(colors: [.pastelPurpleApp, .purple]), startPoint: .leading, endPoint: .trailing)
            )
            .cornerRadius(20)
            
            ScrollView(.horizontal) {
                LazyHStack {
                    if let forecastday = viewModel.state.data?.forecast.forecastday {
                        ForEach(forecastday, id:\.date){ result in
                            DaysView(forecastdayModel: result)
                        }
                    }
                }
                .background(Color.clear)
                .padding()
                
            }
            .background(Color(UIColor.systemGroupedBackground))
            Spacer()
        }
        .padding(.horizontal,16)
        .onAppear {
            viewModel.onAppear(name: name)
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(viewModel: DetailViewModel(dependencies: DetailViewModel
            .InputDependences(detailInteractor: DetailInteractor(depedencies:
                                                                    WeatherRepository()))), name: "London")
    }
}
