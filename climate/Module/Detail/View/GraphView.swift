//
//  GraphView.swift
//  climate
//
//  Created by dpolania on 13/06/23.
//

import SwiftUI
import Charts

struct GraphView: View {
    var temperatureGraph: [TemperatureGraph]
    var body: some View {
        Chart(temperatureGraph) { data in
            LineMark(x: .value("Hora", data.time),
                     y: .value("Temperatura", data.temp_c))
            
        }
    }
}

struct GraphView_Previews: PreviewProvider {
    static var previews: some View {
        GraphView(temperatureGraph: [TemperatureGraph(temp_c: 10.0, time: "1"),
                                     TemperatureGraph(temp_c: 12.0, time: "2"),
                                     TemperatureGraph(temp_c: 13.0, time: "3"),
                                     TemperatureGraph(temp_c: 20.0, time: "4")
                                    ])
    }
}
