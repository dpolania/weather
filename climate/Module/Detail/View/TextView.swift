//
//  TextView.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import SwiftUI
import Resolver

struct TextView<ViewModel> : View where ViewModel : SearchViewModelType {
    @State var navigatioButton = false
    @State var name = ""
    @State var location = ""
    @StateObject var viewModel: ViewModel
    @State var viewDidLoad = true
    
    var body: some View {
        let binding = Binding<String>(get: {
            self.name
        }, set: {
            self.name = $0
        })
        return VStack {
            VStack {
                            HStack {
                                TextField(
                                    "", text: $location
                                )
                                .onChange(of: location) {
                                    print($0)
                                    viewModel.state.textSearch = $0
                                }
                                .foregroundColor(.white)
                                .accentColor(.white)
                            }
                        }
                        .textFieldStyle(BottomLineTextFieldStyle()).foregroundColor(.white)
                        .frame(maxWidth: .infinity, maxHeight: 60)
                        .padding(.top, 40)
                        .padding(.horizontal, 24)
                        .background(Color.primaryApp)
            NavigationLink(destination: DetailView(viewModel: Resolver.resolve(), name: "London"),isActive: $navigatioButton) {
                Button {
                    navigatioButton = true
                } label: {
                    Text("Prueba")
                }
            }
        }.onAppear {
            if viewDidLoad == true {
                viewDidLoad = false
                viewModel.onAppear()
            }
        }
    }
}

//struct TextView_Previews: PreviewProvider {
//    static var previews: some View {
//        TextView()
//    }
//}
