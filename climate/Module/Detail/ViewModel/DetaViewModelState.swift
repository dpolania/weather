//
//  DetaView.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import Foundation
import SwiftUI

final class DetailViewModelState: ObservableObject {
    @Published var data: ForecastResponseModule?
    @Published var viewMessage : Bool = false
    @Published var errorMessage : String = ""
}
