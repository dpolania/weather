//
//  DetailViewModel.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import Foundation
import Resolver
import Combine
import SwiftUI

protocol DetailViewModelType: ObservableObject {
    var state: DetailViewModelState { get set }
    func getInfoForecast(_ name: String)
    func onAppear(name: String)
}

class DetailViewModel: BaseViewModel, DetailViewModelType {
    
    struct InputDependences {
        var detailInteractor : DetailInteractorType
    }
    
    @ObservedObject var state: DetailViewModelState = DetailViewModelState()
    private var subsriptions = Set<AnyCancellable>()
    private let collectTimeStride = 1
    private var dependencies : InputDependences
    
    init(dependencies : InputDependences){
        self.dependencies = dependencies
    }
    
    func onAppear(name: String) {
        getInfoForecast(name)
    }
    
    func getInfoForecast(_ name: String) {
        dependencies.detailInteractor.getDeatilWeather(name: name.replacingOccurrences(of: " ", with: "%20"))
            .receive(on: DispatchQueue.main)
            .sink {response in
                guard case .failure(let error) = response else {
                    return
                }
                self.updateState {
                    (self.state.viewMessage, self.state.errorMessage) = self.evaluateError(error) ?? (false,"")
                }
            } receiveValue: { [weak self] data in
                guard let self = self else { return }
                self.updateState {
                    self.state.data = data
               }
            }
            .store(in: &subsriptions)
    }
    
    private func updateState(updater : () -> Void) {
        updater()
        self.objectWillChange.send()
    }
}
