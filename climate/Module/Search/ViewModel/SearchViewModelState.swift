//
//  SearchViewModelState.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import Foundation
import SwiftUI

class SearchViewModelState: ObservableObject {
    @Published var textSearch: String = ""
    @Published var loadPage: Bool = false
    @Published var data: [SearchResponseModel] = [SearchResponseModel]()
    @Published var viewMessage : Bool = false
    @Published var errorMessage : String = ""
    @Published var name: String = ""
}
