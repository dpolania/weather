//
//  SearchViewModel.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import Foundation
import Resolver
import Combine
import SwiftUI

protocol SearchViewModelType: ObservableObject {
    var state: SearchViewModelState { get set }
    func onAppear()
    func setupSearchObservable()
    func search(text: String)
}

class SearchViewModel: BaseViewModel, SearchViewModelType {
    
    struct InputDependences {
        var searchInteractor : SearchInteractorType
    }
    
    var state: SearchViewModelState = SearchViewModelState()
    private var subsriptions = Set<AnyCancellable>()
    private let collectTimeStride = 1
    private var dependencies : InputDependences
    
    init(dependencies : InputDependences){
        self.dependencies = dependencies
    }
    
    func onAppear() {
        setupSearchObservable()
    }
    
    func setupSearchObservable() {
        state.$textSearch
            .debounce(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .sink { text in
                    self.search(text: text.replacingOccurrences(of: " ", with: "%20"))
        }
        .store(in: &subsriptions)
    }
    
    func search(text: String) {
        state.loadPage = false
        guard !text.isEmpty else {
            self.updateState {
                self.state.data = [SearchResponseModel]()
            }
            return
        }
        dependencies.searchInteractor.getPlaceSearch(search: text)
            .receive(on: DispatchQueue.main)
            .sink {response in
                guard case .failure(let error) = response else {
                    return
                }
                self.updateState {
                    (self.state.viewMessage, self.state.errorMessage) = self.evaluateError(error) ?? (false,"")
                }
        } receiveValue: { [weak self] data in
            self?.updateState {
                self?.state.data = data
                self?.state.loadPage = true
            }
        }
        .store(in: &subsriptions)
    }
    
    private func updateState(updater : ()->Void){
        updater()
        self.objectWillChange.send()
    }
}
