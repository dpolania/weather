//
//  SearchResponseModel.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation

struct SearchResponseModel: Codable {
    let id: Int
    let name: String
    let region: String
    let country: String
    let lat: Double
    let lon: Double
    let url: String
}
