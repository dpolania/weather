//
//  CardLocationFind.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import SwiftUI

struct CardLocationFind: View {
    var name: String
    var region: String
    var country: String
    let colorRandom = ColorRandom()
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(name)
                    .font(.custom("Righteous-Regular.",
                                  size: 28))
                    .foregroundColor(.black)
                HStack(alignment:.bottom) {
                    Text(country)
                        .font(.custom("Montserrat-Medium",
                                      size: 16))
                        .foregroundColor(.black)
                    
                    Text(region)
                        .font(.custom("Montserrat-Thin",
                                      size: 12))
                        .foregroundColor(.black)
                }
            }
            .padding(.leading,24)
            Spacer()
            LottieView(name: "done", loopMode: .playOnce, onAnimation: {_ in}, progress: 1)
                .frame(width: 60, height: 60)
                .padding(.trailing)
        }
        .frame(height: 80)
        .background(colorRandom.getColor())
        .cornerRadius(20)
        .listRowBackground(Color.clear)
    }
}

struct CardLocationFind_Previews: PreviewProvider {
    static var previews: some View {
        CardLocationFind(name: "Bogota",
                         region: "America del sur",
                         country: "Colombia")
            .previewLayout(.fixed(width: 400.0, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
            
    }
}
