//
//  SearchView.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import SwiftUI
import Resolver
import Combine

struct SearchView<ViewModel> : View where ViewModel : SearchViewModelType {
    @StateObject var viewModel: ViewModel
    @State var name = ""
    @State var viewDidLoad = true
    
    var body: some View {
        VStack {
            VStack {
                HStack {
                    TextField(
                      "Search",
                      text: $viewModel.state.textSearch
                    )
                    .foregroundColor(.white)
                    .accentColor(.white)
                }
            }
            .textFieldStyle(BottomLineTextFieldStyle()).foregroundColor(.white)
            .frame(maxWidth: .infinity, maxHeight: 60)
            .padding(.top, 40)
            .padding(.horizontal, 24)
            .background(Color.primaryApp)
            let results = viewModel.state.data
            if results.count > 0 && viewModel.state.loadPage {
                List {
                    ForEach(results, id:\.id){ result in
                        NavigationLink(destination: DetailView(viewModel: Resolver.resolve(), name: result.name)) {
                            CardLocationFind(name: result.name,
                                             region: result.region,
                                             country: result.country)
                        }
                        .listRowBackground(Color.clear)
                    }
                    
                }
                .background(Color.clear)
                .listRowBackground(Color.clear)
            } else {
                LottieView(name: "searchmap",loopMode:.loop) { complete in
                    print(complete)
                }
                .frame(maxHeight: .infinity)
            }
        }
        .alert(viewModel.state.errorMessage, isPresented: $viewModel.state.viewMessage) {
            Button("Aceptar", role: .cancel) {
                viewModel.state.textSearch = ""
            }
        }
        .ignoresSafeArea()
        .background(Color.white)
        .onAppear {
            if viewDidLoad == true {
                viewDidLoad = false
                viewModel.onAppear()
            }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(viewModel: SearchViewModel(
            dependencies: SearchViewModel
                .InputDependences(searchInteractor:
                                    SearchInteractor(depedencies: WeatherRepository())
                                 )
            )
        )
    }
}

