//
//  SplashView.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import SwiftUI
import Lottie
import Combine
import Resolver

struct SplashView: View {
    @State private var isShowingHomeView = false
    var body: some View {
        NavigationView {
            NavigationLink(destination: destination, isActive: $isShowingHomeView) {
                LottieView(loopMode:.playOnce) { complete in
                    isShowingHomeView = true
                }
            }.navigationBarHidden(true)
                .background(Color.primaryApp)
        }
    }
    
    @ViewBuilder
    var destination : some View {
        SearchView<SearchViewModel>(viewModel: Resolver.resolve())
        .navigationBarHidden(true)
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
