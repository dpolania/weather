//
//  DetailInteractor.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import Foundation
import Combine
import Resolver

protocol DetailInteractorType {
    func getDeatilWeather(name: String) -> AnyPublisher<ForecastResponseModule, Error>
}

final class DetailInteractor : DetailInteractorType {
    private var weatherRepository : WeatherRepositoryType
    private let days = 3
    
    init(depedencies: WeatherRepositoryType) {
        weatherRepository = depedencies
    }
    
    func getDeatilWeather(name: String) -> AnyPublisher<ForecastResponseModule, Error>{
        return weatherRepository.getDeatilWeather(text: name, days: days)
    }
}

