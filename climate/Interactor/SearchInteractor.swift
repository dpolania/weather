//
//  SearchInteractor.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import Foundation
import Combine
import Resolver

protocol SearchInteractorType {
    func getPlaceSearch(search: String) -> AnyPublisher<[SearchResponseModel], Error>
}

final class SearchInteractor : SearchInteractorType {
    private var weatherRepository : WeatherRepositoryType
    
    init(depedencies: WeatherRepositoryType) {
        weatherRepository = depedencies
    }
    
    func getPlaceSearch(search: String) -> AnyPublisher<[SearchResponseModel], Error>{
        return weatherRepository.getPlaceSearch(search: search)
    }
}
