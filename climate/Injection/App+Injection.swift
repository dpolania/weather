//
//  App+Injection.swift
//  climate
//
//  Created by dpolania on 9/06/23.
//

import Foundation
import Resolver
import Combine


extension Resolver: ResolverRegistering {
  public static func registerAllServices() {
      defaultScope = .unique
      register{JSONDecoder()}
      register{NetworUtil()}
      register{Set<AnyCancellable>()}
      register{WeatherRepository()}.implements(WeatherRepositoryType.self)
      resolverSearchView()
      resolverDetailView()
  }
}
