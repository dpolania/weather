//
//  Injection+Detail.swift
//  climate
//
//  Created by dpolania on 12/06/23.
//

import Foundation
import Resolver

extension Resolver{
    static func resolverDetailView(){
        resolverDetailInteractor()
        resolverDetailViewModel()
    }
    
    static func resolverDetailInteractor(){
        register{DetailInteractor(depedencies: Resolver.resolve())}.implements(DetailInteractorType.self)
    }
    
    static func resolverDetailViewModel() {
        register(DetailViewModel.self){ resolver in
            let depedencies = DetailViewModel(dependencies: DetailViewModel
                .InputDependences(detailInteractor: resolver.resolve(DetailInteractorType.self))
            )
            return depedencies
        }
    }
}
