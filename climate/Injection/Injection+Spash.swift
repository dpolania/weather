//
//  Injection+Spash.swift
//  climate
//
//  Created by dpolania on 10/06/23.
//

import Foundation
import Resolver

extension Resolver{
    static func resolverSearchView(){
        resolverSearchInteractor()
        resolverSearchViewModel()
    }
    
    static func resolverSearchInteractor(){
        register{SearchInteractor(depedencies: Resolver.resolve())}.implements(SearchInteractorType.self)
    }
    
    static func resolverSearchViewModel() {
        register(SearchViewModel.self){ resolver in
            let depedencies = SearchViewModel(dependencies: SearchViewModel
                .InputDependences(searchInteractor: resolver.resolve(SearchInteractorType.self))
            )
            return depedencies
        }
    }
}

